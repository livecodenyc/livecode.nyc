---
title: February Algorave
location: ALPHAVILLE
date: 2018-02-13 8:00 PM
image: /image/2018-february-algorave.png
link: https://www.facebook.com/events/159771637999316
---

Time for more bass, beats and booleans.

==FEATURING SIGHTS AND SOUNDS BY==

Codie

Reckoner

messica arson

DEWLY WED

Scorpion Mouse

==Tickets==

$10

==More about Algoraves==

[https://algorave.com/](https://algorave.com/)

==Code of Conduct==

[https://livecode.nyc/coc.html](https://livecode.nyc/coc.html)

