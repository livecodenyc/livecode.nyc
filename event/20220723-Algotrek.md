---
title: Algotrek
location: 🌄
date: 2022-07-23 9:15 AM
duration: 12:00
image: /media/logo-white.svg 
categories: [hike, code, trek, music, art, computers]
---

# livecode goes hiking

We will be hiking a small mountain together to make art at the top! The summit is approximately 1,500 feet high. Anyone can do this hike, but it is not always easy and the first hour may be difficult for some. If you come along just take your time and we will make sure you get to the top.
The hike takes approximately five and a half hours. Bring lunch and a lot of water, we'll carry extra water as well.
It's going to be cold and it might snow!!! Wear boots and dress appropriately!

### This is a top-sneaky operation. If you're interested in joined please contact organizers via discord

