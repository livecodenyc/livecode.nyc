---
title: Light Mode at Algoclub
location: The Cell Theatre, 338 West 23rd Street, New York NY, 10011
date: 2022-07-30 11:00 AM
duration: 8:00
image: /image/2022-algoclub-lightmode.jpg
link: https://www.fluxfactory.org/event/algoclub-workshops/
categories: [algorave, club, music, art, computers, workshops, learning]
---

# Enable Light Mode
Have you ever wanted to make electronic dance music? Or some psychedelic video art? Did you know it can be as elegant as writing words, like writing a poem?
Learn livecoding techniques and processes from our community's performers and experts. Get started with audio tools like Sonic Pi and TidalCycles. Or come learn how to create audio reactive visuals with tools like Hydra and GLSL.

[More info and tickets available here](https://www.eventbrite.com/cc/algoclub-light-mode-workshops-862019)

## Workshops

### Intro to Digital Video Synthesis with Hydra with Cameron Alexander (emptyflash) - 11:00

[Tickets](https://www.eventbrite.com/e/algoclub-light-mode-workshop-intro-to-digital-video-synthesis-with-hydra-tickets-386908552897)

We will explore the Hydra video synthesizer, going over the basics and some advanced topics. Helpful to have some understanding of javascript but not required.

### Beatmaking w/ Sonic Pi with Roxanne Harris (alsoknownasrox) - 12:15

[Tickets](https://www.eventbrite.com/e/algoclub-light-mode-workshop-beatmaking-w-sonic-pi-tickets-386927579807)

We will cover how to trigger synths, play audio files, and apply FX in Sonic Pi. Additionally, we will explore concepts like sample slicing and randomness to generate music.

### Intro to Shaders for Programmers with Char Stiles (charstiles) - 13:30

[Tickets](https://www.eventbrite.com/e/algoclub-light-mode-workshop-enough-music-theory-for-live-coding-tickets-386942955797)

In this workshop, you will learn how to livecode shaders by creating a visual composition with GPU code (GLSL) and a little bit of math. Livecoding is an ideal way to create music visualizers, or any interactive abstract graphics of the sort. You will learn about where the shader exists in the graphics pipeline, the basics of how the language works, and how to quickly iterate on writing your shader using livecoding tools. You will also be given a plethora of tools to build upon what you learn, so you can keep learning and practicing beyond this workshop.

### Enough Music Theory For Live Coding with Azhad Syed (azhadsyed) - 15:45

[Tickets](https://www.eventbrite.com/e/algoclub-light-mode-workshop-enough-music-theory-for-live-coding-tickets-386942955797)

This will be a three part workshop.

(30 min) In part 1, we'll cover some of the fundamentals of music theory that you need to remix popular songs. No music theory experience is required! We'll cover how to find the key of a song, how to write a bass line and harmonies, and once you're comfortable with those, how to extend harmonies and find your own sound.

(30 min) In part 2, we'll talk about some of the limitations of popular western music theory, and what other systems of music theory exist in different musical cultures. We'll also review some ways to achieve music based on alternative music theories in TidalCycles.

We'll use the last 30 minutes as an open workshop time for participants to try out the tools gained through the masterclass, and even do some show and tell.

### Sound as Collage: Mixed Media Zine Making with Sabrina Sims (starlybri) - 17:30

[Tickets](https://www.eventbrite.com/e/algoclub-light-mode-workshop-sound-as-collage-mixed-media-zine-making-tickets-386951541477)

We will be making zines inspired by livecoding, dance music and other sounds. Zines are low cost, diy publications that anyone can make. They are a call and response to culture & events just like the club and dance scene. Participants will learn how to make a booklet zine using collage elements and poetry to explore and express our connections to music.  We will have a zine reading/show and tell at the end. Supplies provided, feel free to bring any paper/found objects you want use.  All levels of zine/art experience welcome!
