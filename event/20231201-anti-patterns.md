---
title: Anti-Patterns
location: Hex House
date: 2023-12-01 6:30 PM
duration: 4:00
image: /image/anti_patterns.png
link: https://partiful.com/e/505GdZjhbuv9xnwCjLcA
categories: [algorave, music, art, computers]
feed:
    enable: true
---
# ANTI-PATTERNS 

As part of Festival Tsonami 2023, ANTI PATTERNS presents a collaborative and immersive live-coding performance. This event is broadcasted live from New York City to Chile and features 8 artists from Livecode.NYC. They come together for an hour-long sonic exploration using open-source programming tools, creating space for improvisation and the emergence of Anti Patterns.


This program is commissioned in partnership with our broadcasting ally, Wave Farm, who provides livestream support to make it all possible.


For those unable to attend in person, you can listen in on [Wavefarm Radio](https://wavefarm.org/listen)


Lineup:

snow

emptyflash

Switch Angel

Hexe.exe

Reckoner

v10101A

Messica Arson

Voyde
