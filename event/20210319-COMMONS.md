---
title: COMMON Music festival
location: currents.fm
date: 2021-03-19 7:00 PM
link: https://common.currents.fm/
image: /image/COMMON.jpg
---

Melody Loveless / Colonel Panix
Kengchakaj
Messica Arson
Maxwell Neely-Cohen / gwenpri.me
Luisa Mei / Char Stiles
Dan Gorelick / Solequemal
EEEEAAII / Haute.rod
Sophia Sanborn / Indira Ardolic
Andrew Cotter / lzkpla
Codenobi & Wookie
