---
title: 7 Deadly Sines
location: Wonderville, 1186 Broadway, Brooklyn, NY 11221
date: 2024-06-15 8:00 PM
duration: 5:30
image: /image/2024-7-deadly-sines.png
categories: [algorave, music, art, computers, pride, queer]
link: https://withfriends.co/event/20217204/7_deadly_sines
---

# 7 Deadly Sines

This Pride month, livecode.NYC would like to spotlight the other deadly sins with a night of queer livecode music and visuals. Join us for an evening filled with the 7 deadly sines, from wrathful amplitudes to slothful wavelengths and maybe a tangent or two.

**Performances by:**
Missy
gwenprime
Sara Adkins
Marzipan
Alanza
janie jaffe
&RU
this.xor.that
hexe.exe
Jay Reinier
Switch Angel
LO.FI.SCI.FI
v10101a
luciform
keyboard cowboy
The Rebel Scum, aka: Codenobi and Wookie