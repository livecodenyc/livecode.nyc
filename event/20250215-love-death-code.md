---
title: 'Love Death Code'
location: Wonderville
date: 2025-02-15 7:00 PM
duration: 6:00
link: https://withfriends.co/event/22715914/love_death_code
image: /image/love_death_code.png
categories: [algorave, music, art, computers]
---

On Saturday February 15th, join @livecodenyc for Love Death Code: our HALLOWEEN ON VALENTINES tradition. Feel equal parts ❤️ love ❤️ and 💀 terror 💀 at the intersection of 😱 art 😱 and 💻 technology 💻! Come for the performances, stay for the 👻 COSTUME CONTEST 👻~ Our categories are loveliest, deathliest, and most computer.

🌹 with sets by 🌹
* & laelu {@laelume}
* voyde {@voydescreamsback} & Xenon Chameleon {@xenon_chameleon}
* swan {@swan.trombone} & emptyflash {@emptyflash}
* LOFI-SCIFI {@lo.fi.sci.fi} & Switch Angel {@_switch_angel}
* Øhh {@Olgaakop} & jeffistyping {@jeffistyping}
* JPEGSTRIPES {@jpegstripes} & Jeff Wave {@Vaporwave.obj}
* Varsity Star {@varsity.star}
* Molly Million & MrSynAckSter {@mrsynackster}
* Quantum Connor {@Quantum.connor} & Nicolai {@nickolaslai}
