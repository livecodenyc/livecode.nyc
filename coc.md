---
Title: Code of Conduct
---

# The Livecode.NYC Code of Conduct

[[toc]]

All participants in the Livecode.NYC community, including discord server, meetups, performances, and other events are required to agree with the following code of conduct. Organizers will enforce this code online and in person.

## Short Version

Livecode.NYC is dedicated to providing harassment-free discord, meetup, performance, and event experiences for everyone, regardless of gender, gender identity and expression, age, sexual orientation, disability, physical appearance, body size, race, ethnicity, religion (or lack thereof), or technology choices. We do not tolerate harassment of community members in any form. Degrading sexual language and imagery is not appropriate in any Livecode.NYC context, including emails to the list, discord, performances, workshops, and the meetups. Members  violating these rules may be sanctioned or expelled from the community at the discretion of the organisers.

## Long Version

### Expected Behavior

* Be kind to others

* Be patient
* Productive communication requires effort. Think about how your words will be interpreted.
* Remember that sometimes it is best to refrain entirely from commenting
* Do not insult or put down other community members
* Be respectful of people’s boundaries and space

We do not tolerate harassment based on race, gender, religion, age, color, national origin, physical appearance, disability, sexual orientation, or gender identity.

Harassment includes, but is not limited to:

* Unwelcome or hostile behavior
* Speech that intimidates, creates discomfort, or interferes with a person's participation in the community
* Unwelcome physical contact
* Unwelcome sexual attention
* Deliberate intimidation
* Stalking
* Spamming, trolling, flaming
* Posting (or threatening to post) other people’s personally identifying information (doxing)
* Microaggressions: brief and commonplace verbal, behavioral and environmental indignities that communicate hostile, derogatory or negative slights and insults to a person or group.
* Becoming overly intoxicated at shows
* Illegal behavior including drug use and underage drinking
* Physically violent behavior i.e. pushing, harsh moshing, punching or fighting
* Symbols of hatred which can include hats, pins, patches, buttons or shirts with offensive imagery or hateful sayings
* Negative heckling Harassing photography, recording, or using any Livecode.NYC images in haressing ways
* Sustained or willful disruption of events, programming, or services
* Advocating for or encouraging any of the above behavior

### Who is bound by the Code of Conduct?

* Everyone on the discord server
* Everyone attending meetups
* Everyone performing at an event
* Everyone attending an event

Private harassment is also unacceptable. No matter who you are, if you feel you have been or are being harassed or made uncomfortable by a community member, please contact one of the organizers immediately. Whether you’re a long-time member or a newcomer, we care about making this community a safe place for you and we’ve got your back.

### What if someone violates the Code of Conduct?

* Document as much as you can: time, place, people involved, and what happened.
* Please report the violation immediately in person to an organizer.

### What kinds of action might be taken?

* Asking a violator to apologize for their actions
* Issuing warnings to violators
* Holding meetings with violators to discuss future participation
* Suspending a violator from the mailing list and/or discord
* Removing a violator from an event
* Banning a violator from future events
* We at Livecode.NYC believe in restorative justice, and understand that each individual faces unique struggles. The intention is not to punish people, but trying our best to mediate harm and moving forward together.

### Who are the organizers?

The organizers enforce this code of conduct and decide what is and is not a violation. You should contact them if you feel a community member is behaving in a way that violates anything mentioned in this document.

#### Discord

* gwenprime @gwenprime
* Ramsey Nasser @doomoftherock

#### Algoraves

Shows are organized by different people. Check the show flyer or web page for the information on the Livecode.NYC and venue organizers. 

### Social Rules

In addition, Livecode.NYC members are expected to follow the[ Social Rules of Recurse Center](https://www.recurse.com/social-rules). From their document:

> The social rules are lightweight. You should not be afraid of breaking a social rule. These are things that everyone does, and breaking one doesn’t make you a bad person. If someone says, "hey, you just feigned surprise," or "that’s subtly sexist," don’t worry. Just apologize, reflect for a second, and move on.

## Changing The Rules

This Code of Conduct can be changed if the community decides to.

*This version of the Code of Conduct was drafted by Jessica Garson, May Cheung, and Ramsey Nassser.*