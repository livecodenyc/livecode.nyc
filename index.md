---
title: livecode.nyc
hero:
    title: Livecode.NYC
    subtitle: The Livecoding Community of New York City
    video_src: /media/livecodenyc.mp4

---

# About

Livecode.NYC is a New York City-based artist collective devoted to the real-time programming technique called livecoding, and exploring its potential as a methodology of artistic expressions. This leaderless collective has no formal membership or hierarchy; anyone is welcome to join, attend, and host events. Our members include artists, engineers, actors, designers, educators, musicians, game developers, and writers. We view live coding as a process-driven practice and not specific to a particular medium or language. Our work has been featured in [The New York Times](https://www.nytimes.com/2019/10/04/style/live-code-music.html), [VICE](https://www.vice.com/en_us/article/j5wgp7/who-killed-the-american-demoscene-synchrony-demoparty), [The Financial Times](https://www.ft.com/content/0b64ec04-c283-11e9-ae6e-a26d1d0455f4), and [Alternative Press](https://www.altpress.com/algorave-live-coding-scene-explained/)

Our regular meetups occur every month and are open to all. We also host periodic workshops, talks, festivals, and have performed at various venues across the city. If you would like to get involved, or find out more about our forthcoming events, please [join our discord](https://discord.gg/rR5WNJncaa)!

**About Livecoding**

Livecoding and [Algoraves](https://algorave.com/) (algorithm + rave) are global movements. While traditional music and visual production processes are effectively opaque, a key component to livecoding is transparency, displaying the code and interfaces that produce the sights and sounds in real time. Some guiding principles of our community include: Exposing algorithmic processes, challenging established institutions and hierarchies, collaborative efforts with other communities, equitable practices of diversity and inclusion, and making space for experimentation and failure.
