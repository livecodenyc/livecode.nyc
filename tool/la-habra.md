---
name: La Habra
image: https://github.com/sarahgp/la-habra/raw/master/docs/la-habra.gif
link: https://github.com/sarahgp/la-habra
---

La Habra is a Clojurescript—Figwheel—SVG—Electron app. That means the graphics code is written in Clojurescript, which is live-reloaded by Figwheel and run in an Electron app window. It uses the SVG and CSS animations APIs to draw.

La Habra is used by [Codie](https://codie.live/) and made by [Sarah](/member/sarah.html).
