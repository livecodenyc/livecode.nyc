---
name: Indira Ardolic (voyde)
image: https://live.staticflickr.com/65535/54303092104_97f5562b28_h.jpg
links:
    website: https://readymag.website/u3425633850/4568584/
    instagram: https://www.instagram.com/voydescreamsback/
    youtube: https://www.youtube.com/@indira7217/
---

Livecodes visuals using Unreal Engine. Creates video art, documentaries, games, interactive experiences, and installations.